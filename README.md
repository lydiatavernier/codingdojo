# CodingDojo

## Pré-requis / Installation :

Installations de Robot Framework pour windows :

* Installation de python 3 et librairies python pour robot --> https://www.python.org/downloads/release/python-373/
* pip install robotframework
* pip install robotframework-requests
* pip install robotframework-appiumlibrary


Installation de Java (Windows Hors ligne (64 bits)) : https://www.java.com/fr/download/manual.jsp

Installation de RED (RED_0.9.4.20200428111032-win32.win32.x86_64.zip) : https://github.com/nokia/RED/releases/tag/0.9.4

Paramétrage : 
* Ouvrir les paramètres windows
* Chercher "modifier les variables"
* Choisir "Modifier les variables d'environnement système
* Cliquer sur "Variables d'environnement..."
*  Pour tous les test Robot Framework modifier la variable path en ajoutant <chemin vers python> et <chemin vers python>\Scripts
Rq : les même variables d'environnements sont à setter dans votre .profile si vous travailler sous Linux / Mac

## Documents utiles
Sites sur RF : 
* https://robotframework.org/
* Le user guide et standard library : http://robotframework.org/robotframework/#user-guide

* La documentation de la librairie Requests
* https://github.com/bulkan/robotframework-requests
* http://bulkan.github.io/robotframework-requests/
* La documentation de la librairie Appium
* https://github.com/serhatbolsu/robotframework-appiumlibrary
* http://serhatbolsu.github.io/robotframework-appiumlibrary/AppiumLibrary.html
* Un article sur RF : https://latavernedutesteur.fr/2020/02/12/pourquoi-robot-framework-alexis-pallier/



-----------------------------------------------------------------------
Pour un prochain coding dojo Robot Framework Appium :

Installation de appium desktop (Appium-windows-1.13.0.exe) : https://github.com/appium/appium-desktop/releases/tag/v1.13.0

Installation de Android Studio (android-studio-ide-183.5522156-windows.exe ) : https://developer.android.com/studio/?gclid=CjwKCAjwx_boBRA9EiwA4kIELpehBYjiPrkE6bBCiX4Htdagy6TN0kVg2i_161dMyR1D5bmumtpDERoCto0QAvD_BwE#downloads 

