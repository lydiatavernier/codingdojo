*** Settings ***
Resource    keywords/kw.robot
Library     lib/lib.py

*** Test Cases ***
Test avec kw local
    KeywordLocal    toto

Test avec kw provenant de resource
    KeywordResource    toto

Test avec kw provenant de lib et kw provenant de resource
    ${var}=   generate_string
    KeywordResource   ${var}

*** Keywords ***
KeywordLocal
    [Arguments]    ${parametre}
    Should Be Equal As Strings  ${parametre}    tutu
